## SHRT.OVH ##

Nowe narzedzie umozliwiajace zapisanie wielu linkow pod postacia jednego.

Uzytkownik ma mozliwosc utworzenia kolekcji linkow ktore beda zapisane pod postacia jednego linku kierujacego do Serwisu.

Odbiorca linku po kliknieciu na wygenerowany link, zostanie przekierowany do Serwisu. Przygotowany kod otworzy wszystkie linki z kolekcji w nowych kartach.