# Generator-XDD Application #

Simple generator for memes.

Build with [Slim3 PHP Framework](https://www.slimframework.com/).

### Set up your own meme-machine ###
To set your own machine you will need to configure the Application with file `./application/config/site.php`, Individual sites at `./sites/<appId>/config/` and paste your images into `./application/public/assets/images/source/`
the hostnames are associatied with appId in file `./sites/sites.json`


#### Requirements ####
* PHP 7.2
* PHP-GD extension installed.
* [Composer](https://getcomposer.org/)


In order to run the Application on your local machine:
* download the repository
* navigate to the project root 
* run `composer update`
* run CLI command `php -S localhost:8080 -t application/public/` from the project root directory
* in your browser open the url `localhost:8080`



#### PHP workers
All applicaitions shares `supervisord` workers. Entry in `/etc/supervisor/conf.d/process_image.conf` should execute `php /var/www/application/bin/process_image.php /generate`
The image generation process will 


#### Cache ####
By default the ConfigCache for application and sites is stored in the `./application/data/cache/`.  See  for settings at `./application/src/settings.php`

##### cron jobs #####
There is cron job to run on daily basis. The job removes images older than 2 days. The job is scripted at `./application/bin/rm_images.php`

##### Frontend #####



Installation instructions for `node` and grunt tasks are included inside the Vagrant file

To create your own bootstrap please go to [online editor](https://pikock.github.io/bootstrap-magic/app/index.html#!/editor)
and replace file `./build/frontend/styles/_custom.scss` with generated variables. You can also edit that file manually.

The issue tracker is located at [https://bitbucket.org/generator-xdd/generator-xdd/issues](https://bitbucket.org/generator-xdd/generator-xdd/issues)


Development environment is described in [README.VM.md](./README.VM.md)


#### What is missing ####
* csrf
* prg
* captcha
* unit tests

##### Bugs/fixes #####
* interval on ajax load, if loading an image has failed then retry for 30 seconds
* remove images older than 2 weeks
* if image file does not exists but there is entry in DB, then generate img
* proper git flow with configs
* frontend compilation
* documentation