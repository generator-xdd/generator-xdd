# -*- mode: ruby -*-
# vi: set ft=ruby :

VAGRANTFILE_API_VERSION = '2'

@script_tvp = <<SCRIPT

# Install dependencies

export APP_ENV=development
export DEBIAN_FRONTEND=noninteractive

sudo apt-get install software-properties-common
sudo apt-get update -y

sudo apt-get install -y apache2
#sudo apt-get install -y git
sudo apt-get install -y zip unzip
sudo apt-get install -y curl

apt-get install -y php7.2
apt-get install -y php7.2-bcmath php7.2-bz2 php7.2-cli php7.2-curl php7.2-intl php7.2-json php7.2-mbstring php7.2-gd

apt-get install -y php-xdebug

apt-get install -y php7.2-sqlite

apt-get install -y php-memcached memcached

# Configure Apache
echo "<VirtualHost *:80>
	DocumentRoot \"/var/www/sites/pasek-tvp/public\"
	AllowEncodedSlashes On

	ServerName "pasek-tvp.local.vm";
	ServerAlias "www.pasek-tvp.local.vm";

	<Directory \"/var/www/sites/pasek-tvp/public\">
		Options +Indexes +FollowSymLinks
		DirectoryIndex index.php index.html
		Order allow,deny
		Allow from all
		AllowOverride All
	</Directory>

#    RewriteEngine on
#    RewriteCond %{REQUEST_FILENAME} !-d
#    RewriteCond %{REQUEST_FILENAME} !-f
#    RewriteRule . index.php [L]

	ErrorLog /var/www/sites/pasek-tvp/logs/apache2-error.log
   	CustomLog /var/www/sites/pasek-tvp/logs/apache2-access.log combined

</VirtualHost>" > /etc/apache2/sites-available/000-default.conf

a2ensite 000-default

a2enmod rewrite

# XDEBUG CONFIGURATION
echo "\n\r[xdebug]" >> /etc/php/7.2/mods-available/xdebug.ini
echo "xdebug.default_enable: 1" >> /etc/php/7.2/mods-available/xdebug.ini
echo "xdebug.remote_autostart: 0\n" >> /etc/php/7.2/mods-available/xdebug.ini
echo "xdebug.remote_connect_back: 1\n" >> /etc/php/7.2/mods-available/xdebug.ini
echo "xdebug.remote_enable=1\n" >> /etc/php/7.2/mods-available/xdebug.ini
echo "xdebug.remote_handler: dbgp\n" >> /etc/php/7.2/mods-available/xdebug.ini
echo "xdebug.remote_port: '9000'\n" >> /etc/php/7.2/mods-available/xdebug.ini
echo "xdebug.remote_host: '127.0.0.1:8010'\n" >> /etc/php/7.2/mods-available/xdebug.ini
echo "xdebug.idekey: 'PHPSTORM'\n" >> /etc/php/7.2/mods-available/xdebug.ini
echo "xdebug.remote_mode: 'req'\n" >> /etc/php/7.2/mods-available/xdebug.ini
echo "xdebug.var_display_max_depth: '-1'\n" >> /etc/php/7.2/mods-available/xdebug.ini
echo "xdebug.var_display_max_children: '-1'\n" >> /etc/php/7.2/mods-available/xdebug.ini
echo "xdebug.var_display_max_data: '-1'\n" >> /etc/php/7.2/mods-available/xdebug.ini

# START APACHE2
service apache2 restart

rm -Rf /var/www/html

# Install Composer
if [ -e /usr/local/bin/composer ]; then
    /usr/local/bin/composer self-update
else
    curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
fi

cd ~

# Reset home directory of vagrant user
if ! grep -q "cd /var/www" /home/vagrant/.profile; then
    echo "cd /var/www" >> /home/vagrant/.profile
fi
echo 'ls -la' >> /home/vagrant/.profile

service memcached start

sudo apt-get install -y beanstalkd
sudo apt-get install -y supervisor
echo "[program:process_image]
command=php /var/www/application/bin/process_image.php /generate
autostart=true
autorestart=true
numprocs=3
process_name=process_image-worker-%(process_num)s
" > /etc/supervisor/conf.d/process_image.conf
sudo service supervisor reload
sudo service supervisor restart

# Install Adminer.php
#cd /var/www/build/bin
#sh adminer.sh

### Install the Composer dependencies ###
cd /var/www/application/ && composer install

cd /var/www/

rm ./data/database/meme_basic-xdd.sqlite3
touch ./data/database/meme_basic-xdd.sqlite3

rm ./data/database/content-xdd.sqlite3
touch ./data/database/content-xdd.sqlite3

cd /var/www/application/
php ./vendor/bin/phinx migrate -c phinx_meme_basic.yml -e production
php ./vendor/bin/phinx migrate -c phinx_content.yml -e production
php ./vendor/bin/phinx seed:run -c phinx_content.yml -e production

cd /var/www/sites/pasek-tvp
php ./vendor/bin/phinx seed:run -c phinx_content.yml -e production
php ./vendor/bin/phinx seed:run -c phinx_meme_basic.yml -e production

### Install Node and Frontend Manager ###

#sudo apt-get install -y nodejs

#npm install gulp-cli -g
#npm install gulp -D

#curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
#echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list
#sudo apt-get update && sudo apt-get install -y yarn
#yarn

######

echo "** Visit http://localhost:8010 ot http://pasek-tvp.local.vm in your browser for to view the application **"
SCRIPT

Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|

    config.vm.synced_folder './application', '/var/www/application', id:"application-root",owner:"vagrant",group:"www-data",mount_options:["dmode=775,fmode=664"]
    config.vm.synced_folder './data', '/var/www/data', id:"application-data",owner:"vagrant",group:"www-data",mount_options:["dmode=775,fmode=664"]

    config.vm.define "tvp" do |tvp|
        tvp.vm.box = "ubuntu/bionic64"
        tvp.vm.network "forwarded_port", guest: 80, host: 8010
        tvp.vm.network "forwarded_port", guest: 22, host: 8022
        tvp.vm.network :private_network, :auto_network => true
        tvp.vm.synced_folder './sites', '/var/www/sites', id:"site-tvp-root",owner:"vagrant",group:"www-data",mount_options:["dmode=775,fmode=664"]

        tvp.vm.provision 'shell', inline: @script_tvp
        tvp.vm.hostname = 'pasek-tvp.local.vm'

        tvp.vm.provider "virtualbox" do |vb|
            vb.customize ["modifyvm", :id, "--memory", "768"]
            vb.customize ["modifyvm", :id, "--name", "TVP XDD"]
        end
    end

end
