<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use \Application\InputFilter\InputFilterFactory;
use Ramsey\Uuid\Exception\UnsatisfiedDependencyException;
use Ramsey\Uuid\Uuid;

require  __DIR__ . '/../vendor/autoload.php';

if (PHP_SAPI == 'cli')
{

    $argv = $GLOBALS['argv'];
    array_shift($argv);
    $pathInfo = implode('/', $argv);
    $env = \Slim\Http\Environment::mock(['REQUEST_URI' => $pathInfo]);

    // Read settings and start the SlimApp
    $settings = require __DIR__ . '/../src/settings.php';
    $settings['environment'] = $env;
    $app = new \Slim\App($settings);

    // Set up dependencies
    require __DIR__ . '/../src/dependencies.php';

    // Register middleware
    require __DIR__ . '/../src/middleware.php';

    $container = $app->getContainer();
    $container['errorHandler'] = function ($container) {
        return function ($request, $response, $exception) use ($container) {
            print('error');
            exit(1);
        };
    };
    $container['notFoundHandler'] = function ($container) {
        return function ($request, $response) use ($container) {
            print('not_found');
            exit(1);
        };
    };

    $app->get('/generate', function ($request, $response, $args) {

        $args['app_config'] = $this->app_config->getApplicationConfig();

        $reader = new Zend\Config\Reader\Json();
        $data   = $reader->fromFile(__DIR__ . "/../../sites/sites.json");

        $sitesAvailable = [];

        if(array_key_exists('instance',$data)&&array_key_exists('hosts',$data['instance'])) {
            foreach($data['instance']['hosts'] as $hostIndex=>$hostValue) {
                $sitesAvailable[$hostIndex] = $hostIndex;
                $args['site_config'][$hostIndex] = $this->site_config->setAppId($hostIndex)->prepareSiteConfig()->getSiteConfig();
            }
        }

//        $args['site_config'] = $this->site_config->setAppId('pasek-tvp')->prepareSiteConfig()->getSiteConfig();
//        var_dump($args['site_config']);
//        die();
        $this->job_queue->watch('generate');

        while($job = $this->job_queue->reserve()) {

            $received = json_decode($job->getData(), true);

            $data = $received['data'];


            $declaredInputSpec = $args['site_config'][$received['app_uid']]['site'][$received['app_uid']]['input'];
            $specNamePattern = $args['site_config'][$received['app_uid']]['site'][$received['app_uid']]['output']['character']['requested_image_path'];

            // Load the (requested) base image from filepath
            foreach($declaredInputSpec['character'] as $characterDeclaration) {
                if($characterDeclaration['value']===(int)$data['character']) {
                    $args['requested_image_path'] = sprintf($specNamePattern,$characterDeclaration['value']);
                    break;
                } else {
                    echo 'dupa';
                }
            }
            $response_image_filename = null;

            $image = imagecreatefrompng(__DIR__ . "/../../sites/".$received['app_uid']."/public/" . $args['requested_image_path']);

            foreach($declaredInputSpec['text'] as $textInput) {

                $inputName = $textInput['name'];

                $specOutput = $args['site_config'][$received['app_uid']]['site'][$received['app_uid']]['output'][$inputName];

                $specOutputCoordinates = $specOutput['coordinates'];

                $specOutputColor = $specOutput['color'];

                $response_image_filename .= md5(mb_strtoupper($data[$inputName]));

                $colorArr = $args['site_config'][$received['app_uid']]['site'][$received['app_uid']]['output'][$inputName]['color'];

                $color[$inputName] = imagecolorallocate($image, $colorArr['red'],$colorArr['green'],$colorArr['blue']);

                // Generate text
                $generatedText = imagecolorallocate($image, $specOutputColor['red'], $specOutputColor['green'], $specOutputColor['blue']);
                $font_path = __DIR__ . "/../../sites/".$received['app_uid']."/public/" . $args['site_config'][$received['app_uid']]['site'][$received['app_uid']]['output'][$inputName]['font'];

                // Apply text to the image
                imagettftext(
                    $image,
                    $specOutput['font_size'],
                    0,
                    $specOutputCoordinates['x'],
                    $specOutputCoordinates['y'],
                    $generatedText,
                    $font_path,
                    mb_strtoupper($data[$inputName])
                );
            }

            $base_filename_path = explode('/', $args['requested_image_path']);
            $base_filename = array_pop($base_filename_path);

            // generate filename for the target image
            $response_image_filename = $received['item_uid'] . '.png';

            // fetch filesystem path for new image
            $args['response_image_path'] = __DIR__ . "/../../sites/".$received['app_uid']."/public/" . "/g/" . $response_image_filename;

            // create an image
            imagepng($image,$args['response_image_path']);

            // free up the memory
            imagedestroy($image);

//        var_dump($received);

            $this->job_queue->delete($job);

//        while($job = $this->job_queue->reserve()) {
//            $received = json_decode($job->getData(), true);
//            if(isset($received['data'])) {
//                $data = $received['data'];
//            } else {
//                $data = array();
//            }
//            echo "Received a (" . current($data) . ") ...";
//        }
        }




        return 'update!';
    });
    $app->run();
} else {
    echo 'nah';
}
