#!/bin/bash

echo "[program:process_image]
command=php /var/www/application/bin/process_image.php /generate
autostart=true
autorestart=true
numprocs=3
process_name=process_image-worker-%(process_num)s
" > /etc/supervisor/conf.d/process_image.conf
sudo service supervisor reload
sudo service supervisor restart