<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use \Application\InputFilter\InputFilterFactory;
use Ramsey\Uuid\Exception\UnsatisfiedDependencyException;
use Ramsey\Uuid\Uuid;

require  __DIR__ . '/../vendor/autoload.php';

if (PHP_SAPI == 'cli')
{
    $jsonReader = new Zend\Config\Reader\Json();
    $sitesConfig = $jsonReader->fromFile(__DIR__ . '/../../sites/sites.json');

    if( array_key_exists('instance',$sitesConfig)
        && array_key_exists('hosts',$sitesConfig['instance'])) {
        foreach($sitesConfig['instance']['hosts'] as $appId => $instanceAliases) {
            $filesPath = sprintf("./../../sites/%s/public/g/*",$appId);
            $files = glob($filesPath);
            $now   = time();
            foreach ($files as $file) {
                if (is_file($file)) {
                    if ($now - filemtime($file) >= 60 * 60 * 24 * 2) { // 2 days
                        unlink($file);
                    }
                }
            }
        }
    }
}