<?php
/**
 * @link      https://bitbucket.org/generator-xdd/generator-xdd for the canonical source repository
 * @copyright Copyright (c) 2018 Secalith Ltd
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\InputFilter;

use Zend\InputFilter\InputFilter;

class InputFilterFactory
{
    protected $inputFilter;

    /**
     * InputFilterFactory constructor.
     */
    public function __construct($inputFilterConfig=null,$rawData = [])
    {
        // load vendors Input Filter class
        $inputFilter = new InputFilter();

        foreach ($inputFilterConfig as $inputFilterConfigGroup) {
            foreach ($inputFilterConfigGroup as $inputFilterConfig) {
                $inputFilter->add($inputFilterConfig);
            }
        }

        if( ! empty($rawData)) {
            $inputFilter->setData($rawData);
        }

        $this->inputFilter = $inputFilter;

    }


    public function getInputFilter()
    {
        return $this->inputFilter;
    }
}