<?php

namespace Application\ABC\Model;

class CommonModel
{
    public $data;
    public $block;

    public function exchangeArray($data)
    {
        $this->data = (isset($data['data']))?isset($data['data']):null;
        $this->block = (isset($data['block']))?isset($data['block']):null;
    }
}