<?php

namespace Application\ABC\Service;

class BlockService {

    protected $block_table_gateway;
    protected $content_table_gateway;

    public function __construct(
        \Zend\Db\TableGateway\TableGateway $block_table_gateway,
        \Zend\Db\TableGateway\TableGateway $content_table_gateway
    ) {
        $this->block_table_gateway = $block_table_gateway;
        $this->content_table_gateway = $content_table_gateway;
    }

    public function getRootBlocksByAreaIdAndParentId($area_id=null,$parent_id=null,$currentI18n='pl_pl')
    {
        $return=null;

        $hydrator = new \Zend\Hydrator\ObjectProperty();
        $hydrator->addStrategy('attributes', new \Application\ABC\Hydrator\JSONStrategy());
        $hydrator->addStrategy('parameters', new \Application\ABC\Hydrator\JSONStrategy());
        $hydrator->addStrategy('options', new \Application\ABC\Hydrator\JSONStrategy());

        $where = [];

        if(null!==$area_id) {
            $where['area_uid']=$area_id;
        }

        $where['parent_uid']=$parent_id;

        $selectBlockResults = $this->block_table_gateway->select($where);

        $result = $selectBlockResults->buffer();

        if(count($result)>0) {
            foreach($result as $blockRow) {
                $return[$blockRow->uid]['data'] = new \ArrayObject($hydrator->extract($blockRow));
                if( ! empty($return[$blockRow->uid]['data'])) {

                    $c = $this->getContentByBlockIdAndLanguage($blockRow->uid,$currentI18n);
                    if($c!==null) {
                        $return[$blockRow->uid]['content'] = $c;
                    }
                    $b = $this->getRootBlocksByAreaIdAndParentId($area_id,$blockRow->uid,$currentI18n);
                    if($b!==null) {
                        $return[$blockRow->uid]['block'] = $b;
                    }
                }
            }
        }

        return $return;
    }

    public function getContentByBlockIdAndLanguage($block_id,$currentI18n='pl_pl')
    {
        $return=null;

        $hydrator = new \Zend\Hydrator\ObjectProperty();
        $hydrator->addStrategy('attributes', new \Application\ABC\Hydrator\JSONStrategy());
        $hydrator->addStrategy('parameters', new \Application\ABC\Hydrator\JSONStrategy());
        $hydrator->addStrategy('options', new \Application\ABC\Hydrator\JSONStrategy());

        $selectContentResults = $this->content_table_gateway
            ->select(
                [
                    'block_uid'=>$block_id,
//                    'language'=>$currentI18n,
                ]
            );
        if(count($selectContentResults)>0) {
            foreach ($selectContentResults as $contentRow) {
                $return[$contentRow->uid]['data'] = new \ArrayObject($hydrator->extract($contentRow));
                $c = $this->getContentByParentContentIdAndLanguage($contentRow->uid,$currentI18n);
                if(null!==$c) {
                    $return[$contentRow->uid]['content'] = $c;
                }
            }
        }

        return $return;
    }

    public function getContentByParentContentIdAndLanguage($content_id,$currentI18n='pl_pl')
    {
        $return=null;

        $hydrator = new \Zend\Hydrator\ObjectProperty();
        $hydrator->addStrategy('attributes', new \Application\ABC\Hydrator\JSONStrategy());
        $hydrator->addStrategy('parameters', new \Application\ABC\Hydrator\JSONStrategy());
        $hydrator->addStrategy('options', new \Application\ABC\Hydrator\JSONStrategy());

        $selectContentResults = $this->content_table_gateway
            ->select(
                [
                    'parent_uid'=>$content_id,
//                    'language'=>$currentI18n,
                ]
            );
        if(count($selectContentResults)>0) {
            foreach ($selectContentResults as $contentRow) {
                $return[$contentRow->uid]['data'] = $hydrator->extract($contentRow);
//                $return[$contentRow->uid]['content'] = $this->getContentByParentCotentIdAndLanguage($contentRow->uid,$currentI18n);
            }
        }

        return $return;
    }
}