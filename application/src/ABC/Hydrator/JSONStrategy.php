<?php

namespace Application\ABC\Hydrator;

use Zend\Hydrator\Strategy\StrategyInterface;

class JSONStrategy implements StrategyInterface
{
    public function extract($value, ?object $object = null)
    {
        return \Zend\Json\Json::decode($value);
    }

    public function hydrate($value, ?array $data)
    {
        return $value;
    }
}