<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use \Application\InputFilter\InputFilterFactory;
use Ramsey\Uuid\Exception\UnsatisfiedDependencyException;
use Ramsey\Uuid\Uuid;

$app->get('/', function (Request $request, Response $response, array $args)
{
    $appId = $request->getAttribute('app_id');

    if(null!==$appId && ! empty($appId)) {
        $args['app_config'] = $this->app_config->getApplicationConfig();
        $args['site_config'] = $this->site_config->setAppId($appId)->prepareSiteConfig()->getSiteConfig();

        $layoutFilepath = sprintf(
            "layout/%s.%s",
            $args['site_config']['site'][$appId]['settings']['templates']['route']['home']['name'],
            $args['site_config']['site'][$appId]['settings']['templates']['view_engine']
        );

        return $this->view->render($response, $layoutFilepath, [
            'appId' => $appId,
            'site' => $args['site_config']['site'][$appId],
            'ajax_server' => $request->getAttribute('host'),
        ]);
    } else {
        #TODO: Throw exception

    }

})->setName('homepage_get');

$app->get('/form/{form_name}', function (Request $request, Response $response, array $args)
{
    $form_name = $args['form_name'];

    $appId = $request->getAttribute('app_id');

    if(null!==$appId && ! empty($appId)) {

        $args['app_config'] = $this->app_config->getApplicationConfig();
        $args['site_config'] = $this->site_config->setAppId($appId)->prepareSiteConfig()->getSiteConfig();
        $args['appId'] = $appId;
        $args['brand'] = $args['site_config']['site'][$appId]['brand'];
        $args['settings'] = $args['site_config']['site'][$appId]['settings'];
        $args['basic'] = $args['site_config']['site'][$appId]['basic'];
        $args['input'] = $args['site_config']['site'][$appId]['input'];
        $args['output'] = $args['site_config']['site'][$appId]['output'];

        return $this->renderer->render($response, 'form/' . $form_name . '.phtml', $args);

    } else {
        #TODO: Throw exception
    }

})->setName('homepage_get_form');

/**
 * Display layout only. After rendering the AJAX call will be triggered from the template to the route named `display_public`
 */
$app->get('/u/{item_uid}.png', function (Request $request, Response $response, array $args)
{

    $appId = $request->getAttribute('app_id');

    if(null!==$appId && ! empty($appId))
    {
        $args['app_config'] = $this->app_config->getApplicationConfig();
        $args['site_config'] = $this->site_config->setAppId($appId)->prepareSiteConfig()->getSiteConfig();

        $layoutFilepath = sprintf(
            "layout/%s.%s",
            $args['site_config']['site'][$appId]['settings']['templates']['route']['processed_public']['name'],
            $args['site_config']['site'][$appId]['settings']['templates']['view_engine']
        );

        return $this->view->render($response, $layoutFilepath, [
            'site' => $args['site_config']['site'],
            'ajax_server' => (string) $request->getUri(),
            'path_url' => (string) $request->getUri(),
            'item_uid' => $args['item_uid'],
        ]);

    } else {
        throw new \Slim\Exception\NotFoundException($request, $response);
    }



})->setName('processed_public');

/**
 * Displays image without the html layout
 */
$app->get('/b/{item_uid}[.png]', function (Request $request, Response $response, array $args)
{
    $appId = $request->getAttribute('app_id');

    if(null!==$appId && ! empty($appId))
    {
        $args['app_config'] = $this->app_config->getApplicationConfig();
        $args['site_config'] = $this->site_config->setAppId($appId)->prepareSiteConfig()->getSiteConfig();
        $args['response_image'] = $request->getUri()->getBasePath() . "/g/" . $args['item_uid'] . ".png";
        $args['response_image_filepath'] = __DIR__ . "/g/" . $args['item_uid'];

        if(file_exists($args['response_image_filepath'])) {
            $type = pathinfo($args['response_image_filepath'], PATHINFO_EXTENSION);
            $data = file_get_contents($args['response_image_filepath']);
            if(!$data||empty($data)) {
                throw new \Slim\Exception\NotFoundException($request, $response);
            }
            $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);

            echo $base64;
        } else {
            $itemTable = new Zend\Db\TableGateway\TableGateway('meme_item', $this->db_adapter);
            $results = $itemTable->select(['uid' => $args['item_uid']]);
            if(count($results) > 0) {
                $args['response_image'] = $request->getUri()->getBasePath() . "/b/" . $args['item_uid'] . ".png";

                return $response->withRedirect($args['response_image'], 301);
            } else {
                throw new \Slim\Exception\NotFoundException($request, $response);
            }
        }
    } else {
        throw new \Slim\Exception\NotFoundException($request, $response);
    }

})->setName('display_image_public');
/**
 * Displays image without the html layout
 */
$app->get('/i/{item_uid}[.png]', function (Request $request, Response $response, array $args)
{
    $appId = $request->getAttribute('app_id');

    if(null!==$appId && ! empty($appId))
    {
        $args['app_config'] = $this->app_config->getApplicationConfig();
        $args['site_config'] = $this->site_config->setAppId($appId)->prepareSiteConfig()->getSiteConfig();

        $itemTable = new Zend\Db\TableGateway\TableGateway('meme_item', $this->db_adapter);
        $results = $itemTable->select(['uid' => $args['item_uid']]);

        if(count($results) > 0) {
            $args['response_image'] = $request->getUri()->getBasePath() . "/b/" . $args['item_uid'] . ".png";

            return $response->withRedirect($args['response_image'], 301);
        } else {
            throw new \Slim\Exception\NotFoundException($request, $response);
        }

    } else {
        throw new \Slim\Exception\NotFoundException($request, $response);
    }

})->setName('display_public');

$app->post('/', function (Request $request, Response $response, array $args)
{

    $appId = $request->getAttribute('app_id');

    if(null!==$appId && ! empty($appId))
    {
        $args['app_config'] = $this->app_config->getApplicationConfig();
        $args['site_config'] = $this->site_config->setAppId($appId)->prepareSiteConfig()->getSiteConfig();

        $rawData = $request->getParsedBody();

        $inputFilter = new InputFilterFactory($args['site_config']['site'][$appId]['input'],$rawData);

        if($inputFilter->getInputFilter()->isValid())
        {
            $generatorData = $inputFilter->getInputFilter()->getValues();

            $uuidGen = new \Application\Common\Uuid();

            $newItemUuid = $uuidGen->generateUuid();

            $itemTable = new Zend\Db\TableGateway\TableGateway('meme_item', $this->db_adapter);

            $rowsAfftected['meme_item'] = $itemTable->insert(
                [
                    'uid'=>$newItemUuid,
                    'app_uid'=>$appId,
                    'status'=>0,
                    'created'=>date('Y-m-d H:i:s')
                ]
            );


            foreach($args['site_config']['site'][$appId]['input']['text'] as $textInput)
            {
                $itemTextTable = new Zend\Db\TableGateway\TableGateway('meme_text', $this->db_adapter);
                // save the data to DB
                $itemTextTable->insert([
                    'uid'=>$uuidGen->setUuidGenerator()->generateUuid(),
                    'item_uid'=>$newItemUuid,
                    'text'=>$generatorData[$textInput['name']],
                    'status'=>0,
                    'created'=>date('Y-m-d H:i:s')
                ]);
            }

            // Publish the job for the php image generation workers
            $job = new stdClass();
            $job->data = $generatorData;
            $job->app_uid = $appId;
            $job->item_uid = $newItemUuid;

            // put the data for php worker
            $this->job_queue->useTube('generate')->put(json_encode($job));

            // fetch public url for generated image
            $args['response_image'] = "/u/" . $newItemUuid . ".png";

            // display new image
            return $response->withRedirect($args['response_image'], 301);

        } else {
            $args['form_error'] = $inputFilter->getInputFilter()->getInvalidInput();

            $generator_data = $inputFilter->getInputFilter()->getValues();

            $args['form_data'] = $generator_data;

            $args['form_valid'] = false;

            $args['form_template_selected'] = $generator_data['form_template'];
            $args['form_template'] = $this->renderer->fetch('form/' . $generator_data['form_template'].'.phtml', $args);


            $layoutFilepath = sprintf(
                "layout/%s.%s",
                $args['site']['settings']['templates']['route']['home']['name'],
                $args['site']['settings']['templates']['view_engine']
            );

            return $this->view->render($response, $layoutFilepath, $args);
        }

    } else {
        #TODO: Throw exception

    }

})->setName('homepage_post');

$app->get('/warunki-korzystania-z-serwisu', function (Request $request, Response $response, array $args) {
    // Render index view
    return $this->renderer->render($response, 'terms-and-conditions.phtml', $args);
});