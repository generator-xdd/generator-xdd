<?php
/**
 * @link      https://bitbucket.org/generator-xdd/generator-xdd for the canonical source repository
 * @copyright Copyright (c) 2018 Secalith Ltd
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Twig\Extension;


final class HtmlTagOpen extends \Twig_Extension
{
    protected $standaloneTags = [
        'meta','img'
    ];
    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction('html_spec_open', array($this,'getHtmlSpecOpen')),
        );
    }

    public function getName()
    {
        return 'html_spec_open';
    }

    public function getHtmlSpecOpen($item,$type="all")
    {
        ob_start();
//var_dump($item);
//        switch ($type) {
//            case 'all':
////            default:
//                $attributes = '';
//                if( array_key_exists('options',$item)
//                    && property_exists($item->options,'wrapper')
//                    && property_exists($item->options->wrapper,'outer')
//                ) {
//                    foreach($item->options->wrapper->outer->attributes as $attributeName => $attributeValue) {
//                        if(is_string($attributeValue)) {
//                            $attributes .= $attributeName.'="'.$item->options->wrapper->outer->attributes->{$attributeName}.'"';
//                        } else {
//                            $attr = $attributeName.'="';
//                            foreach($attributeValue as $attrValue) {
//                                $attr .= $attrValue . ' ';
//                            }
//                            $attr = trim($attr);
//                            $attr .= '" ';
//
//                            $attributes .= $attr;
//                        }
//                    }
//                    if( property_exists($item->options->wrapper->outer->parameters,'html_tag')) {
//
//                        if (in_array($item->options->wrapper->outer->parameters->html_tag, $this->standaloneTags)) {
//                            echo sprintf('<%1$s %2$s />', $item->options->wrapper->outer->parameters->html_tag, $attributes);
//                        } else {
//                            echo sprintf('<%1$s %2$s>', $item->options->wrapper->outer->parameters->html_tag, $attributes);
//                        }
//                    }
//                }
//        }

        if(array_key_exists('attributes',$item) && ! empty($item['attributes'])) {
            $attributes = '';
            foreach($item['attributes'] as $attributeName => $attributeValue) {
                if(is_string($attributeValue)) {
                    $attributes .= $attributeName.'="'.$item['attributes']->{$attributeName}.'" ';
                } else {
                    $attr = $attributeName.'="';
                    foreach($attributeValue as $attrValue) {
                        $attr .= $attrValue . ' ';
                    }
                    $attr = trim($attr);
                    $attr .= '"';

                    $attributes .= $attr;
                }
            }
        }

        if(array_key_exists('html_tag',$item['parameters'])) {
            if(in_array($item['parameters']->html_tag,$this->standaloneTags)) {
                echo sprintf('<%1$s %2$s />',$item['parameters']->html_tag,$attributes);
            } else {
                echo sprintf('<%1$s %2$s>',$item['parameters']->html_tag,$attributes);
            }
        } else{
//            var_dumP($item['parameters']);
//            var_dumP($item->parameters);
//            echo 'dupa';
        }

//        switch ($type) {
//            case 'all':
////            default:
//                $attributes = '';
//                // open inner
//                if( array_key_exists('options',$item)
//                    && property_exists($item->options,'wrapper')
//                    && property_exists($item->options->wrapper,'inner')
//                ) {
//                    foreach($item->options->wrapper->inner->attributes as $attributeName => $attributeValue) {
//                        if(is_string($attributeValue)) {
//                            $attributes .= $attributeName.'="'.$item->options->wrapper->inner->attributes->{$attributeName}.'"';
//                        } else {
//                            $attr = $attributeName.'="';
//                            foreach($attributeValue as $attrValue) {
//                                $attr .= $attrValue . ' ';
//                            }
//                            $attr = trim($attr);
//                            $attr .= '"';
//
//                            $attributes .= $attr;
//                        }
//                    }
//                    if( property_exists($item->options->wrapper->inner->parameters,'html_tag')) {
//
//                        if (in_array($item->options->wrapper->inner->parameters->html_tag, $this->standaloneTags)) {
//                            echo sprintf('<%1$s %2$s />', $item->options->wrapper->inner->parameters->html_tag, $attributes);
//                        } else {
//                            echo sprintf('<%1$s %2$s>', $item->options->wrapper->inner->parameters->html_tag, $attributes);
//                        }
//                    }
//                }
//        }

        return ob_get_clean();
    }
}
