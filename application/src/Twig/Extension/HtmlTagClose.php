<?php
/**
 * @link      https://bitbucket.org/generator-xdd/generator-xdd for the canonical source repository
 * @copyright Copyright (c) 2018 Secalith Ltd
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Twig\Extension;


final class HtmlTagClose extends \Twig_Extension
{
    protected $standaloneTags = [
        'meta','img'
    ];
    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction('html_spec_close', array($this,'getHtmlSpecClose')),
        );
    }

    public function getName()
    {
        return 'html_spec_close';
    }

    public function getHtmlSpecClose($item,$type="all")
    {
        ob_start();

        switch ($type) {
            case 'all':
            default:
                // inner
//                if( array_key_exists('options',$item)
//                    && property_exists($item->options,'wrapper')
//                    && property_exists($item->options->wrapper,'inner')
//                    && ! in_array($item->options->wrapper->inner->parameters->html_tag,$this->standaloneTags)
//                ) {
//                    echo sprintf('</%1$s>',$item->options->wrapper->inner->parameters->html_tag);
//                }
                // content
                if( array_key_exists('html_tag', $item['parameters']) && ! in_array($item['parameters']->html_tag,$this->standaloneTags)) {
                    echo sprintf('</%1$s>',$item['parameters']->html_tag);
                }
//                // outer
//                if( array_key_exists('options',$item)
//                    && property_exists($item->options,'wrapper')
//                    && property_exists($item->options->wrapper,'outer')
//                    && ! in_array($item->options->wrapper->outer->parameters->html_tag,$this->standaloneTags)
//                ) {
//                    echo sprintf('</%1$s>',$item->options->wrapper->outer->parameters->html_tag);
//                }
            break;

        }

        return ob_get_clean();
    }
}
