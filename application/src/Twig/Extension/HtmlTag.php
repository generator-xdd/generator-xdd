<?php
/**
 * @link      https://bitbucket.org/generator-xdd/generator-xdd for the canonical source repository
 * @copyright Copyright (c) 2018 Secalith Ltd
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Twig\Extension;


final class HtmlTag extends \Twig_Extension
{
    protected $standaloneTags = [
        'meta'
    ];
    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction('html_spec', array($this,'getHtmlSpec')),
        );
    }

    public function getName()
    {
        return 'html_spec';
    }

    public function getHtmlSpec($item)
    {
        ob_start();

        $attributes = '';

        if(array_key_exists('attributes',$item)) {
            foreach($item['attributes'] as $attribute) {
                $attributes .= sprintf(' %s="%s"',$attribute['name'],$attribute['value']);
            }
        }

        if(in_array($item['html_tag'],$this->standaloneTags)) {
            echo sprintf('<%s %s/>',$item['html_tag'],$attributes);
        } else {
            echo sprintf('<%1$s %2$s>%3$s</%1$s>',$item['html_tag'],$attributes,$item['content']);
        }

        return ob_get_clean();
    }
}
