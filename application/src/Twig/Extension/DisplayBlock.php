<?php
/**
 * @link      https://bitbucket.org/generator-xdd/generator-xdd for the canonical source repository
 * @copyright Copyright (c) 2018 Secalith Ltd
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Twig\Extension;

use Application\Twig\Extension\HtmlTagOpen;
use Application\Twig\Extension\HtmlTagClose;

final class DisplayBlock extends \Twig_Extension
{
    protected $standaloneTags = [
        'meta'
    ];

    public function __construct()
    {
        $this->htmlTagOpen = new HtmlTagOpen();
        $this->htmlTagClose = new HtmlTagClose();
    }

    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction('display_block', array($this,'displayBlock')),
        );
    }

    public function getName()
    {
        return 'display_block';
    }

    public function displayBlock($item)
    {
        ob_start();
        echo $this->htmlTagOpen->getHtmlSpecOpen($item['data']);

        if(array_key_exists('block',$item)) {
            echo $this->displayBlockChildren($item['block']);
        }

        if(array_key_exists('content',$item)) {
            echo $this->displayContent($item['content']);
        }

        echo $this->htmlTagClose->getHtmlSpecClose($item['data']);

        return ob_get_clean();
    }

    public function displayBlockChildren($blocks)
    {
        ob_start();

        foreach($blocks as $childBlock){
            echo $this->displayBlock($childBlock);
        }

        return ob_get_clean();
    }

    public function displayContent($contents)
    {
        ob_start();

        foreach($contents as $content){
            echo $this->htmlTagOpen->getHtmlSpecOpen($content['data']);
            echo $content['data']['content'];
            echo $this->htmlTagClose->getHtmlSpecClose($content['data']);
        }

        return ob_get_clean();
    }

}
