<?php

//$app->add(new \Slim\Csrf\Guard);

//$app = new \Slim\App($container);

$app->add(function ($request, $response, $next) {

//    $this->getContainer()->get('jwt');

//    $response->getBody()->write('BEFORE');
    $response = $next($request, $response);
//    $response->getBody()->write('AFTER');

    return $response;
});

/**
 * Determine Hostname for the current Request
 * Attribute: host
 */
$app->add(function ($request, $response, $next) {

    $uri = Zend\Uri\UriFactory::factory((string)$request->getUri());
    $host = $uri->getHost();

    $request = $request->withAttribute('host', $host);

    $response = $next($request, $response);

    return $response;
});
/**
 * Determine Hostname for the AJAX Requests
 * Attribute: ajax_host
 */
$app->add(function ($request, $response, $next) {

    $uri = Zend\Uri\UriFactory::factory((string)$request->getUri());

    $ajaxHost = sprintf("%s://%s",$uri->getScheme(),$uri->getHost());
    $request = $request->withAttribute('ajax_host', $ajaxHost);

    $response = $next($request, $response);

    return $response;
});
/**
 * Attribute: app_page
 */
$app->add(function ($request, $response, $next) {
    // Determine requested Routename
    $route = $request->getAttribute('route');
    if (empty($route)) {
        throw new \Slim\Exception\NotFoundException($request, $response);
    }

    $routeName = $route->getName();
    $currentInstance = $request->getAttribute('app_instance');
    $pageTable = $this->table_gateway['page'];
    $templateTable = $this->table_gateway['template'];
    // get route and router by route_name, site_uid and method
    $selectPage = $pageTable->getSql()->select();
//var_dump($current->application_uid);
//var_dump($current->site_uid);
//var_dump($routeName);
//var_dump($request->getAttribute('i18n_current'));

//    var_dump($currentInstance);
//    die();

    $selectPage->where(
        [
            sprintf("%s.application_uid",$pageTable->getTable()) => $currentInstance->application_uid,
            sprintf("%s.site_uid",$pageTable->getTable()) => $currentInstance->site_uid,
            sprintf("%s.name",$pageTable->getTable()) => $routeName,
            sprintf("%s.language",$pageTable->getTable()) => $request->getAttribute('i18n_current'),
        ]
    );

    $concatJoin = sprintf("%s.uid = %s.template_uid",$templateTable->getTable(),$pageTable->getTable());
    $selectPage->join($templateTable->getTable(), $concatJoin,['tpl_type'=>'type','tpl_location'=>'location','tpl_name'=>'name'],'left');

    $rowset = $pageTable->selectWith($selectPage);

    $currentPage = $rowset->current();
//var_dump($currentPage);
//die();
    $request = $request->withAttribute('app_page', $currentPage);

    $response = $next($request, $response);

    return $response;

});

/**
 * Get Blocks
 * Attribute: abc
 */
$app->add(function ($request, $response, $next){
    // Determine requested Routename
    $route = $request->getAttribute('route');
    if (empty($route)) {
        throw new \Slim\Exception\NotFoundException($request, $response);
    }

    $areaResultset = $request->getAttribute('app_areas');
    $currentI18n = $request->getAttribute('i18n_current');
    $area=null;

    if(count($areaResultset)>0) {
        foreach ($areaResultset as $areaRow) {

            $hydrator = new \Zend\Hydrator\ObjectProperty();


            $hydrator->addStrategy('attributes', new Application\ABC\Hydrator\JSONStrategy());
            $hydrator->addStrategy('parameters', new Application\ABC\Hydrator\JSONStrategy());
            $hydrator->addStrategy('options', new Application\ABC\Hydrator\JSONStrategy());

            $area[$areaRow->scope][$areaRow->machine_name][$areaRow->uid]['data'] = new \ArrayObject($hydrator->extract($areaRow));

            $blocks = $this['block_service']->getRootBlocksByAreaIdAndParentId($areaRow->uid,null,$currentI18n);

            $area[$areaRow->scope][$areaRow->machine_name][$areaRow->uid]['block'] = $blocks;

        }

        $request = $request->withAttribute('abc', $area);
    }

    $response = $next($request, $response);

    return $response;
});


/**
 * Determine Areas
 * Attribute: app_areas
 */
$app->add(function ($request, $response, $next)
{
    // Determine requested Routename
    $route = $request->getAttribute('route');
    if (empty($route)) {
        throw new \Slim\Exception\NotFoundException($request, $response);
    }

    $routeName = $request->getAttribute('route')->getName();

    $siteId = $request->getAttribute('site_id');

    $areaTable = $this->table_gateway['area'];
    $templateTable = $this->table_gateway['template'];
    $routeTable = $this->table_gateway['route'];
    $routerTable = $this->table_gateway['router'];
    $siteTable = $this->table_gateway['site'];

    $selectArea = $areaTable->getSql()->select();

    $selectArea->columns(
        [
            'uid' => 'uid',
            'machine_name' => 'machine_name',
            'scope' => 'scope',
            'attributes' => 'attributes',
            'parameters' => 'parameters',
            'options' => 'options'
        ]
    );

    $concatJoin = sprintf("%s.template_uid = %s.uid",$areaTable->getTable(),$templateTable->getTable());
    $selectArea->join($templateTable->getTable(), $concatJoin,['template_uid'=>'uid'],'inner');

    $concatJoin = sprintf("%s.route_uid = %s.uid",$templateTable->getTable(),$routeTable->getTable());
    $selectArea->join($routeTable->getTable(), $concatJoin,['route_uid'=>'uid'],'inner');

    $concatJoin = sprintf("%s.uid = %s.route_uid",$routeTable->getTable(),$routerTable->getTable());
    $selectArea->join($routerTable->getTable(), $concatJoin,['router_uid'=>'uid'],'inner');

    $concatJoin = sprintf("%s.site_uid = %s.uid",$routerTable->getTable(),$siteTable->getTable());
    $selectArea->join($siteTable->getTable(), $concatJoin,['site_uid'=>'uid','site_id'],'inner');

    $selectArea->where(array('route.route_name' => $routeName,'site_id'=>$siteId));

//    echo $areaTable->getSql()->getSqlstringForSqlObject($selectArea);
//die();

    $rowset = $areaTable->selectWith($selectArea);


    $areaResultset = $rowset->buffer();

    $request = $request->withAttribute('app_areas', $areaResultset);

    $response = $next($request, $response);

    return $response;
});

/**
 * Determine the current sites language
 * Attribute: i18n_current
 */
$app->add(function ($request, $response, $next) {

    // Determine requested Routename
    $route = $request->getAttribute('route');
    if (empty($route)) {
        throw new \Slim\Exception\NotFoundException($request, $response);
    }

    $request = $request->withAttribute('i18n_current', 'pl_pl');

    $response = $next($request, $response);

    return $response;
});
/**
 * reads content.instance data by site_id param
 * Attribute: app_instance
 */
$app->add(function ($request, $response, $next) {

    // Determine requested Routename
    $route = $request->getAttribute('route');
    if (empty($route)) {
        throw new \Slim\Exception\NotFoundException($request, $response);
    }

    $siteId = $request->getAttribute('site_id');

    $instancesTable = $this->table_gateway['instances'];
    $select = $instancesTable->getSql()->select();
    $select->columns(['uid','site_uid','site_id','application_uid']);
    $select->where(array('site_id' => $siteId));
    $rowset = $instancesTable->selectWith($select);
    $currentInstance =  $rowset->current();

    $request = $request->withAttribute('app_instance', $currentInstance);

    $response = $next($request, $response);

    return $response;

});
/**
 * Determine appId for the current Request
 * Load key in entry instance.hosts from the `./sites/sites.json` file
 * Attribute: site_id
 */
$app->add(function ($request, $response, $next) {

    // Determine requested Routename
    $route = $request->getAttribute('route');
    if (empty($route)) {
        throw new \Slim\Exception\NotFoundException($request, $response);
    }

    $uri = Zend\Uri\UriFactory::factory((string)$request->getUri());
    $host = $uri->getHost();

    // load sites.json
    $jsonReader = new Zend\Config\Reader\Json();
    $sitesConfig = $jsonReader->fromFile(__DIR__ . '/../../sites/sites.json');

    if( array_key_exists('instance',$sitesConfig)
        && array_key_exists('hosts',$sitesConfig['instance'])) {
        foreach($sitesConfig['instance']['hosts'] as $siteId => $instanceAliases) {
            if(in_array($host,$instanceAliases)) {
                $request = $request->withAttribute('site_id', $siteId);
                break;
            }
        }
    }

    $response = $next($request, $response);

    return $response;
});

/**
 * Determine appId for the current Request
 * Load key in entry instance.hosts from the `./sites/sites.json` file
 * Attribute: site_id
 */
$app->add(function ($request, $response, $next) {

    // Determine requested Routename
    $route = $request->getAttribute('route');
    if (empty($route)) {
        throw new \Slim\Exception\NotFoundException($request, $response);
    }

    $uri = Zend\Uri\UriFactory::factory((string)$request->getUri());
    $host = $uri->getHost();

    // load sites.json
    $jsonReader = new Zend\Config\Reader\Json();
    $sitesConfig = $jsonReader->fromFile(__DIR__ . '/../../sites/sites.json');

    if( array_key_exists('instance',$sitesConfig)
        && array_key_exists('hosts',$sitesConfig['instance'])) {
        foreach($sitesConfig['instance']['hosts'] as $siteId => $instanceAliases) {
            if(in_array($host,$instanceAliases)) {
                $request = $request->withAttribute('site_id', $siteId);
                break;
            }
        }
    }

    $response = $next($request, $response);

    return $response;
});




//$app->add(new \Slim\HttpCache\Cache('public', 86400));