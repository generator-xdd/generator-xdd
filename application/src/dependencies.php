<?php
// DIC configuration
$container = $app->getContainer();
// view renderer
$container['renderer'] = function ($c) {
    $settings = $c->get('settings')['renderer'];
    return new Slim\Views\PhpRenderer($settings['template_path']);
};
// monolog
$container['logger'] = function ($c) {
    $settings = $c->get('settings')['logger'];
    $logger = new Monolog\Logger($settings['name']);
    $logger->pushProcessor(new Monolog\Processor\UidProcessor());
    $logger->pushHandler(new Monolog\Handler\StreamHandler($settings['path'], $settings['level']));
    return $logger;
};

/**
 *
 * @param $c
 * @return \Application\Config\ApplicationConfig
 */
$container['app_config'] = function ($c) {
    $settings = $c->get('settings')['app_config'];
    $config = new Application\Config\ApplicationConfig($settings);

    return $config;
};

$container['site_config'] = function ($c) {

    $settings = $c->get('settings')['site_config'];

    $config = new Application\Config\SiteConfig($settings);

    return $config;
};

$container['block_service'] = function ($c) {

    $blockService = new \Application\ABC\Service\BlockService($c['table_gateway']['block'],$c['table_gateway']['content']);

    return $blockService;

};

$container['service_manager'] = function ($c) {

    $config = [
        'validators' => array(
            'invokables' => array(
                'email-address' => 'RKA\Validator\EmailAddress',
                'string-length' => 'RKA\Validator\StringLength',
                // etc.
            ),
        ),
        'form_elements' => array(
            'invokables' => array(
                'Application\RKA\ExampleForm'  => 'Application\RKA\ExampleForm',
            ),
        ),

    ];

    $smConfigurator = new Application\RKA\ServiceManagerConfigurator();
    $serviceManager = $smConfigurator->createServiceManager($config);

    return $serviceManager;

};

$container['view'] = function ($c) {
    
    $view = new \Slim\Views\Twig(__DIR__ . '/../templates', [
        'cache' => '../data/cache/twig/', // relative to index.php
        'debug' => false,
    ]);
    $view->addExtension(new Twig_Extension_Debug());

    $view->addExtension(new Application\Twig\Extension\HtmlTagOpen());
    $view->addExtension(new Application\Twig\Extension\HtmlTagClose());
    $view->addExtension(new Application\Twig\Extension\HtmlTag());
    $view->addExtension(new Application\Twig\Extension\DisplayBlock());

    // Instantiate and add Slim specific extension
    $basePath = rtrim(str_ireplace('index.php', '', $c->get('request')->getUri()->getBasePath()), '/');


    $view->addExtension(new Slim\Views\TwigExtension($c->get('router'), $basePath));

    $viewHelperManager = $c->get('service_manager')->get('ViewHelperManager');
    $renderer = new \Zend\View\Renderer\PhpRenderer();
    $renderer->setHelperPluginManager($viewHelperManager);
    $view->getEnvironment()->registerUndefinedFunctionCallback(
        function ($name) use ($viewHelperManager, $renderer) {
            if (!$viewHelperManager->has($name)) {
                return false;
            }
            $callable = [$renderer->plugin($name), '__invoke'];
            $options  = ['is_safe' => ['html']];
            return new Twig_SimpleFunction($name, $callable, $options);
        }
    );

    return $view;
};



//$container['db'] = function ($c) {
//    $db = $c['settings']['db'];
//    $pdo = new PDO('mysql:host=' . $db['host'] . ';dbname=' . $db['dbname'],
//        $db['user'], $db['pass']);
//    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
//    $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
//    return $pdo;
//};

//
/**
 * Configures zend-db-adapter with use of config 'db_adapter' entry
 * @param $c
 * @return PDO|\Zend\Db\Adapter\Adapter
 */
//$container['db_adapter'] = function ($c) {
//    $driverConfig = $c['settings']['db_adapter'];
//
//    $adapter = new Zend\Db\Adapter\Adapter($driverConfig);
//
//    return $adapter;
//
//    $pdo = new PDO('mysql:host=' . $db['host'] . ';dbname=' . $db['dbname'],
//        $db['user'], $db['pass']);
//    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
//    $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
//    return $pdo;
//};
//
///**
// * Configures zend-db-adapter with use of config 'db_adapter' entry
// * @param $c
// * @return array
// */
//$container['data_source'] = function ($c) {
//
//    $dbAdapter = [];
//
//    $driversConfigs = $c['settings']['data_source'];
//
//    if(is_array($driversConfigs)) {
//        foreach($driversConfigs as $driverConfig) {
//            if($driverConfig['type']=='db_adapter') {
//                $dbAdapter[$driverConfig['name']] = new Zend\Db\Adapter\Adapter($driverConfig);
//            }
//        }
//    }
//
//    return $dbAdapter;
//
//};
/**
 *
 */
$container['table_gateway'] = function ($c) {

    $dbTableGateways = [];

    $tableGateways = $c['settings']['table_gateway'];

    if(is_array($tableGateways)) {
        foreach($tableGateways as $tableGateway) {
            // find matching db_adapter
            foreach($c['settings']['data_source'] as $dbAdapterDriver) {
                // try to find matching `db_adapter` by db_adapter_name=>name attribute
                if($dbAdapterDriver['type']=='db_adapter'
                && $tableGateway['db_adapter_name']===$dbAdapterDriver['name']) {

                    $dbAdapter = new Zend\Db\Adapter\Adapter($dbAdapterDriver);

                    $dbTableGateways[$tableGateway['name']] =
                        new Zend\Db\TableGateway\TableGateway($tableGateway['table_name'],$dbAdapter);

                }
            }
        }
    }

    return $dbTableGateways;

};

/**
 * Beanstalk
 * @param $c
 * @return \Pheanstalk\Pheanstalk
 */
$container['job_queue'] = function ($c) {
    $jobQueueConfig = $c['settings']['job_queue'];
    $queue = new Pheanstalk\Pheanstalk($jobQueueConfig['host']);

    return $queue;
};

$container['memcached'] = function ($c) {

    $mem = new \Memcached();
    $mem->addServer("127.0.0.1", 11211);

    return $mem;
};


// http cache
//$container['cache'] = function () {
//    return new \Slim\HttpCache\CacheProvider();
//};