<?php

namespace Application\RKA;

use Zend\Form\Form;
use Zend\InputFilter\InputFilterProviderInterface;

class ExampleForm extends Form implements InputFilterProviderInterface
{
    public function init()
    {
        $this->add([
            'name' => 'url',
            'options' => [
                'label' => 'Url',
            ],
            'attributes' => [
                'id'       => 'url',
                'class'    => 'form-control',
                'required' => 'required',
            ],
        ]);

        $this->add([
            'name' => 'submit',
            'type' => 'submit',
            'attributes' => [
                'class' => 'btn btn-default',
                'value' => _("Shorten"),
            ],
        ]);
    }

    public function getInputFilterSpecification()
    {
        return [
            'url' => [
                'required' => true,
                'filters'  => [
                    ['name' => 'StringTrim'],
                    ['name' => 'StripTags'],
                ],
                'validators' => [
                    ['name' => 'Uri'],
                ],
            ],
        ];
    }
}