<?php
return [
    'settings' => [
        'displayErrorDetails' => true, // set to false in production
        'addContentLengthHeader' => true, // Allow the web server to send the content-length header
        'determineRouteBeforeAppMiddleware' => true,
        // Renderer settings
        'renderer' => [
            'template_path' => __DIR__ . '/../../application/templates/',
        ],
        // Monolog settings
        'logger' => [
            'name' => 'slim-app',
            'path' => isset($_ENV['docker']) ? 'php://stdout' : __DIR__ . '/../logs/app.log',
            'level' => \Monolog\Logger::DEBUG,
            'bubble' => false,
        ],
        'app_config' => [
            'config_glob_paths' => __DIR__ . '/../config/*.{json,yaml,php}',
            'config_cache_key' => 'app_config_cache_key_%s',
            'config_cache_enabled' => true,
            'cache_dir' => __DIR__ . '/../data/cache/app_config.php',
            'sites_config_path' => __DIR__ . "/../../sites/sites.json",
        ],
        'site_config' => [
            'config_glob_paths' => __DIR__ . '/../../sites/%s/config/*.{json,yaml,php}',
            'config_cache_key' => 'site_config_cache_key_%s',
            'config_cache_enabled' => true,
            'cache_dir' => __DIR__ . '/../data/cache/site_config_%s.php',
        ],
//        'db_adapter' => [
//            'driver'   => 'Pdo_Sqlite',
//            'database' => __DIR__ . '/../../data/database/generator-xdd.sqlite3',
//        ],
        'data_source' => [
            [
                "app_id" => "app-001",
                "site_id" => null,
                'name' => 'meme_basic',
                'type' => 'db_adapter',
                'driver'   => 'Pdo_Sqlite',
                'database' =>  __DIR__ . '/../../data/database/meme_basic-xdd.sqlite3',
            ],
            [
                "app_id" => "app-001",
                "site_id" => null,
                'name' => 'content', // this matches entry in the `table_gateway` config entry
                'type' => 'db_adapter',
                'driver'   => 'Pdo_Sqlite',
                'database' =>  __DIR__ . '/../../data/database/content-xdd.sqlite3',
            ],
        ],
        'table_gateway' => [
            [
                'name' => 'application',
                'table_name' => 'application',
                "db_adapter_name" => "content",
            ],
            [
                'name' => 'instances', // this will be matched with config entry in `data_source`
                'table_name' => 'instances',
                "db_adapter_name" => "content",
            ],
            [
                'name' => 'site',
                'table_name' => 'site',
                "db_adapter_name" => "content",
            ],
            [
                'name' => 'router',
                'table_name' => 'router',
                "db_adapter_name" => "content",
            ],
            [
                'name' => 'route',
                'table_name' => 'route',
                "db_adapter_name" => "content",
            ],
            [
                'name' => 'content',
                'table_name' => 'content',
                "db_adapter_name" => "content",
            ],
            [
                'name' => 'page',
                'table_name' => 'page',
                "db_adapter_name" => "content",
            ],
            [
                'name' => 'template',
                'table_name' => 'template',
                "db_adapter_name" => "content",
            ],
            [
                'name' => 'area',
                'table_name' => 'area',
                "db_adapter_name" => "content",
            ],
            [
                'name' => 'block',
                'table_name' => 'block',
                "db_adapter_name" => "content",
            ],
            [
                'name' => 'content',
                'table_name' => 'content',
                "db_adapter_name" => "content",
            ],
            [
                'name' => 'meme_item',
                'table_name' => 'meme_item',
                "db_adapter_name" => "meme_basic",
            ],
            [
                'name' => 'meme_text',
                'table_name' => 'meme_text',
                "db_adapter_name" => "meme_basic",
            ],
            [
                'name' => 'meme_image',
                'table_name' => 'meme_image',
                "db_adapter_name" => "meme_basic",
            ],
        ],
        'job_queue' => [
            'host' => '127.0.0.1',
        ],
    ],
];
