<?php
/**
 * @link      https://bitbucket.org/generator-xdd/generator-xdd for the canonical source repository
 * @copyright Copyright (c) 2018 Secalith Ltd
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\ImageProcessing;

use Zend\InputFilter\InputFilter;

class ImageProcessing
{
    protected $siteConfig;
    protected $data;

    /**
     * InputFilterFactory constructor.
     */
    public function __construct($siteConfig = [], $data=[])
    {
        $this->siteConfig = $siteConfig;
        $this->data = $data;

    }

    public function prepareData()
    {
        // requested image source
        foreach($this->siteConfig['site']['input']['character'] as $characterDeclaration) {
            if($characterDeclaration['value']===(int)$this->data['character']) {
                $args['requested_image_path'] = sprintf(
                    $this->siteConfig['site']['output']['character']['requested_image_path'],
                    $characterDeclaration['value']
                );
                break;
            }
        }

        // prepare text

        var_dump($args);
    }

    public function generateUid()
    {
        // save data
        try {
            $uuid4 = Ramsey\Uuid\Uuid::uuid4();

            $newUuid = $uuid4->toString();
        } catch (Ramsey\Uuid\Exception\UnsatisfiedDependencyException $e) {
            echo 'Caught exception: ' . $e->getMessage() . "\n";
        }

        return $newUuid;
    }


    public function getInputFilter()
    {
        return $this->inputFilter;
    }

    /**
     * @url http://php.net/manual/en/function.base64-encode.php
     * @param $filename
     */
    public function readImageB64($filename)
    {
        $fh = fopen($filename, 'rb');
        //$fh2 = fopen('Output-File', 'wb');

        $cache = '';
        $eof = false;

        while (1) {

            if (!$eof) {
                if (!feof($fh)) {
                    $row = fgets($fh, 4096);
                } else {
                    $row = '';
                    $eof = true;
                }
            }

            if ($cache !== '')
                $row = $cache.$row;
            elseif ($eof)
                break;

            $b64 = base64_encode($row);
            $put = '';

            if (strlen($b64) < 76) {
                if ($eof) {
                    $put = $b64."\n";
                    $cache = '';
                } else {
                    $cache = $row;
                }

            } elseif (strlen($b64) > 76) {
                do {
                    $put .= substr($b64, 0, 76)."\n";
                    $b64 = substr($b64, 76);
                } while (strlen($b64) > 76);

                $cache = base64_decode($b64);

            } else {
                if (!$eof && $b64{75} == '=') {
                    $cache = $row;
                } else {
                    $put = $b64."\n";
                    $cache = '';
                }
            }

            if ($put !== '') {

                //fputs($fh2, $put);
                //fputs($fh2, base64_decode($put));        // for comparing
            }
        }

//fclose($fh2);
        fclose($fh);

        return $put;
    }
}