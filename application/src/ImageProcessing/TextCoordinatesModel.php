<?php

namespace Application\ImageProcessing;

class TextCoordinatesModel {
    public $position_x;
    public $position_y;
    public $angle;
}