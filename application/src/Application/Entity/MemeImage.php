<?php

namespace Application\Entity;

class MemeImage extends CommonEntity
{
    protected $item_uid;

    protected $image;

    /**
     * @return mixed
     */
    public function getItemUid()
    {
        return $this->item_uid;
    }

    /**
     * @param mixed $item_uid
     * @return MemeImage
     */
    public function setItemUid($item_uid)
    {
        $this->item_uid = $item_uid;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param mixed $image
     * @return MemeImage
     */
    public function setImage($image)
    {
        $this->image = $image;
        return $this;
    }

}