<?php

namespace Application\Entity;

class MemeText extends CommonEntity
{
    protected $item_uid;

    protected $text;

    /**
     * @return mixed
     */
    public function getItemUid()
    {
        return $this->item_uid;
    }

    /**
     * @param mixed $item_uid
     * @return MemeText
     */
    public function setItemUid($item_uid)
    {
        $this->item_uid = $item_uid;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @param mixed $text
     * @return MemeText
     */
    public function setText($text)
    {
        $this->text = $text;
        return $this;
    }

}