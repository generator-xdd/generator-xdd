<?php
/**
 * @link      https://bitbucket.org/generator-xdd/generator-xdd for the canonical source repository
 * @copyright Copyright (c) 2018 Secalith Ltd
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Config;

use Zend\ConfigAggregator\ArrayProvider;
use Zend\ConfigAggregator\ConfigAggregator;
use Zend\ConfigAggregator\PhpFileProvider;
use Zend\ConfigAggregator\ZendConfigProvider;

class ApplicationConfig
{
    protected $appHost;
    protected $applicationConfig;

    /**
     * InputFilterFactory constructor.
     */
    public function __construct($config)
    {
        $applicationConfig = new ConfigAggregator([
            new ArrayProvider([ConfigAggregator::ENABLE_CACHE => $config['config_cache_enabled']]),
            new ZendConfigProvider($config['config_glob_paths']),
        ],$config['cache_dir']);
        $this->applicationConfig = $applicationConfig->getMergedConfig();
    }

    public function getApplicationConfig()
    {
        return $this->applicationConfig;
    }

}