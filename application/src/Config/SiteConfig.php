<?php
/**
 * @link      https://bitbucket.org/generator-xdd/generator-xdd for the canonical source repository
 * @copyright Copyright (c) 2018 Secalith Ltd
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Config;

use Zend\ConfigAggregator\ArrayProvider;
use Zend\ConfigAggregator\ConfigAggregator;
use Zend\ConfigAggregator\PhpFileProvider;
use Zend\ConfigAggregator\ZendConfigProvider;

class SiteConfig
{
    protected $appHost;
    protected $appId;
    protected $siteConfig;
    protected $initialSiteConfig;

    /**
     * InputFilterFactory constructor.
     */
    public function __construct($config)
    {
        $this->initialSiteConfig = $config;
//        $applicationConfig = new ConfigAggregator([
//            new ArrayProvider([ConfigAggregator::ENABLE_CACHE => $config['config_cache_enabled']]),
//            new ZendConfigProvider($config['config_glob_paths']),
//        ],$config['cache_dir']);
//        $this->applicationConfig = $applicationConfig->getMergedConfig();
    }

    public function prepareSiteConfig()
    {

        if(null!==$this->initialSiteConfig && is_array($this->initialSiteConfig)) {
            $config = $this->initialSiteConfig;
            foreach ($this->initialSiteConfig as $configIndex => $configValue) {
                switch ($configIndex) {
                    case 'config_glob_paths':
                        $formattedValue = sprintf($configValue, $this->appId);
                        $config['config_glob_paths'] = $formattedValue;
                        break;
                    case 'config_cache_key':
                        $formattedValue = sprintf($configValue, $this->appId);
                        $config['config_cache_key'] = $formattedValue;
                        break;
                    case 'cache_dir':
                        $formattedValue = sprintf($configValue, $this->appId);
                        $config['cache_dir'] = $formattedValue;
                        break;
                }
            }
        }

        $siteConfig = new ConfigAggregator([
            new ArrayProvider([ConfigAggregator::ENABLE_CACHE => $config['config_cache_enabled']]),
            new ZendConfigProvider($config['config_glob_paths']),
        ],$config['cache_dir']);

        $this->siteConfig = $siteConfig->getMergedConfig();

        return $this;
    }

    public function getSiteConfig()
    {
        return $this->siteConfig;
    }

    /**
     * @param mixed $appHost
     * @return SiteConfig
     */
    public function setHost($appHost)
    {
        $this->appHost = $appHost;
        return $this;
    }

    public function setAppId($appId)
    {
        $this->appId = $appId;
        return $this;
    }

}