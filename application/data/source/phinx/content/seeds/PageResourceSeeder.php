<?php


use Phinx\Seed\AbstractSeed;

class PageResourceSeeder extends AbstractSeed
{
    public function run()
    {
        $data = [
            [
                'uid' => 'page-resource-001',
                'page_uid' => 'page-001',
                'site_uid' => 'site-001',
                'resource_uid' => 'create-001',
                'resource_name' => 'meme_item',
                'resource_type' => 'meme_item',
                'resource_cache' => 0,
                'parameters' => '{}',
                'comm' => 'meme_item for pasek-tvp',
                'status' => 1,
                'created' => date('Y-m-d H:i:s'),
            ],
            [
                'uid' => 'page-resource-002',
                'page_uid' => 'page-002',
                'site_uid' => 'site-002',
                'resource_uid' => 'create-002',
                'resource_name' => 'meme_item',
                'resource_type' => 'meme_item',
                'resource_cache' => 0,
                'parameters' => '{}',
                'comm' => 'meme_item for pasek-tvp',
                'status' => 1,
                'created' => date('Y-m-d H:i:s'),
            ],
        ];

        $table = $this->table('page_resource');
        $table->truncate();
        $table->insert($data)
            ->save();
    }
}
