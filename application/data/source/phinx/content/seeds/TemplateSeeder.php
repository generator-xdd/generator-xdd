<?php


use Phinx\Seed\AbstractSeed;

class TemplateSeeder extends AbstractSeed
{
    public function run()
    {
        $data = [
            [
                'uid' => 'template-001',
                'route_uid' => 'route-001',
                'type' => 'filesystem',
                'location' => 'page-view',
                'name' => 'page-default',
                'label' => 'GET pasek-tvp',
                'comm' => 'homepage_get tvp',
                'status' => 1,
                'created' => date('Y-m-d H:i:s'),
            ],
            [
                'uid' => 'template-002',
                'route_uid' => 'route-003',
                'type' => 'filesystem',
                'location' => 'page-view',
                'name' => 'page-default',
                'label' => 'GET pasek-tvp',
                'comm' => 'processed_public tvp',
                'status' => 1,
                'created' => date('Y-m-d H:i:s'),
            ],
            [
                'uid' => 'template-003',
                'route_uid' => 'route-004',
                'type' => 'filesystem',
                'location' => 'page-view',
                'name' => 'page-default',
                'label' => 'GET pasek-tvn',
                'comm' => 'homepage_get tvn',
                'status' => 1,
                'created' => date('Y-m-d H:i:s'),
            ],
            [
                'uid' => 'template-004',
                'route_uid' => 'route-006',
                'type' => 'filesystem',
                'location' => 'page-view',
                'name' => 'page-default',
                'label' => 'GET pasek-tvn',
                'comm' => 'processed_public tvn',
                'status' => 1,
                'created' => date('Y-m-d H:i:s'),
            ],
        ];

        $table = $this->table('template');
        $table->truncate();
        $table->insert($data)
            ->save();
    }
}
