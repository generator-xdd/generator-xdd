<?php


use Phinx\Seed\AbstractSeed;

class ContentSeeder extends AbstractSeed
{
    public function run()
    {
        $data = [

            [
                'uid' => 'content-200',
                'parent_uid' => null,
                'block_uid' => 'block-201',
                'template_uid' => null,
                'type' => 'text',
                'content' => 'Pasek TVP',
                'attributes' => '{"class":"cover-heading pb-3 mt-3"}',
                'parameters' => '{"html_tag":"h1"}',
                'options' => '{}',
                'language' => 'pl_pl',
                'comm' => 'homepage for pl_pl at xdd.local.vm',
                'status' => 1,
                'order' => 1,
                'created' => date('Y-m-d H:i:s'),
            ],
            [
                'uid' => 'content-201',
                'parent_uid' => null,
                'block_uid' => 'block-202',
                'template_uid' => null,
                'type' => 'text',
                'content' => null,
                'attributes' => '{"class":"fb-like float-right align-bottom","data-href":"https://pasek-tvp.pl/", "data-layout":"button_count", "data-action":"like", "data-size":"large", "data-show-faces":"false"}',
                'parameters' => '{"html_tag":"div"}',
                'options' => '{}',
                'language' => 'pl_pl',
                'comm' => 'homepage for pl_pl at xdd.local.vm',
                'status' => 1,
                'order' => 1,
                'created' => date('Y-m-d H:i:s'),
            ],
            [
                'uid' => 'content-210',
                'parent_uid' => null,
                'block_uid' => 'block-206',
                'template_uid' => null,
                'type' => 'text',
                'content' => 'Pasek TVP',
                'attributes' => '{"class":"nav-link", "id":"version-extended-tab", "aria-controls":"version-extended","aria-selected":"true","href":"#version-extended","role":"tab","data-toggle":"tab", "onClick":"javascript:applicationForm.loadForm(\'version-extended\',this);window.location.hash = \'form:\' + this.getAttribute(\'href\').slice(1);"}',
                'parameters' => '{"html_tag":"a"}',
                'options' => '{}',
                'language' => 'pl_pl',
                'comm' => 'homepage for pl_pl at xdd.local.vm',
                'status' => 1,
                'order' => 1,
                'created' => date('Y-m-d H:i:s'),
            ],
            [
                'uid' => 'content-211',
                'parent_uid' => null,
                'block_uid' => 'block-208',
                'template_uid' => null,
                'type' => 'text',
                'content' => 'Pasek TVN',
                'attributes' => '{"class":"nav-link", "id":"version-extended-tab", "href":"https://pasek-tvn.pl"}',
                'parameters' => '{"html_tag":"a"}',
                'options' => '{}',
                'language' => 'pl_pl',
                'comm' => 'homepage for pasek-tvp',
                'status' => 1,
                'order' => 1,
                'created' => date('Y-m-d H:i:s'),
            ],
        ];

        $table = $this->table('content');
        $table->truncate();
        $table->insert($data)
            ->save();
    }
}
