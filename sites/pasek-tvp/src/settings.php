<?php
return [
    'settings' => [
        'data_source' => [
            [
                "app_id" => "app-001",
                "site_id" => null,
                'name' => 'meme_basic',
                'type' => 'db_adapter',
                'driver'   => 'Pdo_Sqlite',
                'database' =>  __DIR__ . '/../../../data/database/meme_basic.sqlite3',
            ],
        ],
        'table_gateway' => [
            [
                'name' => 'shrt_item',
                'table_name' => 'item',
                "db_adapter_name" => "shrt_item",
            ],
            [
                'name' => 'shrt_item_hits_log',
                'table_name' => 'item_hits_log',
                "db_adapter_name" => "shrt_item",
            ],
        ],
    ],
];
