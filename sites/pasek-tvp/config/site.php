<?php
return [
    'site' => [
        'pasek-tvp' => [
            'settings' => [
                'app_id' => 'pasek-tvp',
                'templates' => [
                    'route' => [
                        // usually the files located at ./application/templates
                        'home' => ['name'=>'index-extended'],
                        'processed_public' => ['name'=>'index-processed-extended'],
//                    'home' => ['name'=>'index-experimental'],
                    ],
                ],
            ],
            'basic' => [
//            'form_default' => 'basic',
                'form_default' => 'extended',
                'head' => [
                    'title' => 'Pasek TVP',
                    'description' => 'Generator paska TVP. Strona nie ma zwi&#261;zku z TVN, celem jest parodia ich propagandy.',
                    'keywords' => 'generator pasków tvp, pasek-tvp.pl, pasek tvp pl, pasek tvp, paski tvp generator, pasek tvp generator, tvp generator, generator pasków,generator paskow tvp, paski grozy'
                ],
                'favicon' => [
                    'path' => '/assets/images/favicons/1/'
                ],
                'og' => [
                    [
                        'html_tag' => 'meta',
                        'attributes' => [
                            ['name'=>'property','value'=>'og:image:width'],
                            ['name'=>'content','value'=>279]
                        ],
                    ],
                    [
                        'html_tag' => 'meta',
                        'attributes' => [
                            ['name'=>'property','value'=>'og:image:height'],
                            ['name'=>'content','value'=>279]
                        ],
                    ],
                    [
                        'html_tag' => 'meta',
                        'attributes' => [
                            ['name'=>'property','value'=>'og:title'],
                            ['name'=>'content','value'=>_("Generator paska Widomo&#347;ci TVP.")]
                        ],
                    ],
                    [
                        'html_tag' => 'meta',
                        'attributes' => [
                            ['name'=>'property','value'=>'og:description'],
                            ['name'=>'content','value'=>_("Generator paska TVP. Strona nie ma zwi&#261;zku z TVN, celem jest parodia ich propagandy.")]
                        ],
                    ],
                    [
                        'html_tag' => 'meta',
                        'attributes' => [
                            ['name'=>'property','value'=>'og:url'],
                            ['name'=>'content','value'=>'https://pasek-tvp.pl']
                        ],
                    ],
                    [
                        'html_tag' => 'meta',
                        'attributes' => [
                            ['name'=>'property','value'=>'og:image'],
                            ['name'=>'content','value'=>'https://pasek-tvp.pl/assets/images/og/1/og-image.jpg']
                        ],
                    ],
                    [
                        'html_tag' => 'meta',
                        'attributes' => [
                            ['name'=>'property','value'=>'og:type'],
                            ['name'=>'content','value'=>'website']
                        ],
                    ],
                ],
                'html' => [
                    'title' => 'Pasek TVP',
                    'main' => [
                        'select_character' => [
                            'text' => '1. Wybierz posta&#263;:'
                        ],
                    ],
                    'footer' => [
                        [
                            'html_tag' => 'span',
                            'content' => 'Strona nie ma zwi&#261;zku z TVP, celem jest parodia ich propagandy.',
                            'attributes' => [
                                ['name'=>'class','value'=>'text-muted']
                            ],
                        ],
                        [
                            'html_tag' => 'a',
                            'content' => 'Warunki korzystania z serwisu',
                            'attributes' => [
                                ['name'=>'href','value'=>'/warunki-korzystania-z-serwisu']
                            ],
                        ],
                    ],
                ],
            ],
            'input' => [
                'character' => [
                    [
                        'name' => 'character',
                        'required' => true,
                        'filters' => [
                            ['name' => 'StringTrim']
                        ],
                        'validators' => [
                            [
                                'name' => 'NotEmpty'
                            ],
                            'strlen' => [
                                'name' => 'StringLength',
                                'options' => [
                                    'min' => 1,
                                    'max' => 1
                                ],
                            ]
                        ],
                        'value' => 1,
                        'image_placeholder' => '/assets/images/source/source-1_sample.png',
                        'image_placeholder_url_path' => '/assets/images/source/',
                    ],
                    [
                        'name' => 'character',
                        'required' => true,
                        'filters' => [
                            ['name' => 'StringTrim']
                        ],
                        'validators' => [
                            [
                                'name' => 'NotEmpty'
                            ],
                            'strlen' => [
                                'name' => 'StringLength',
                                'options' => [
                                    'min' => 1,
                                    'max' => 1
                                ],
                            ]
                        ],
                        'value' => 2,
                        'image_placeholder' => '/assets/images/source/source-2_sample.png',
                        'image_placeholder_url_path' => '/assets/images/source/',
                    ]
                ],
                'text' => [
                    [
                        'name' => 'text_1',
                        'required' => true,
                        'label' => '2. Wpisz tre&#347;&#263; paska:',
                        'filters' => [
                            ['name' => 'StringTrim']
                        ],
                        'validators' => [
                            [
                                'name' => 'NotEmpty'
                            ],
                            'strlen' => [
                                'name' => 'StringLength',
                                'options' => [
                                    'min' => 1,
                                    'max' => 48
                                ],
                            ]
                        ]
                    ],
                ],
                'various' => [
                    [
                        'name' => 'form_template',
                        'required' => true,
                        'validators' => [
                            'inarray' => [
                                'name' => 'InArray',
                                'options' => [
                                    'haystack' => [
                                        'version-basic',
                                        'version-extended',
                                    ],
                                ],
                            ]
                        ]
                    ],

                ],
            ],
            'output' => [
                'character' => [
                    'requested_image_path' => '/assets/images/source/source-%s.png',
                    'dimensions' => [
                        'height' => 582,
                        'width' => 1024,
                    ],
                ],
                'text_1' => [
                    'color' => [
                        'red' => 255,
                        'green' => 255,
                        'blue' => 255,
                    ],
                    'font' => '/assets/fonts/franklin_gothic_medium_regular.ttf',
                    'font_size' => 24,
                    'coordinates' => [
                        'x' => 190,
                        'y' => 487
                    ],
                ],
            ],
        ],
    ],
];