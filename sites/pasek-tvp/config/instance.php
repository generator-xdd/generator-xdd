<?php
return [
    'site' => [
        'pasek-tvp' => [
            'settings' => [
                'templates' => [
                    'route' => [
                        'home' => ['name'=>'index-experimental'],
                        'homepage_post' => ['name'=>'index-experimental'],
                        'processed_public' => ['name'=>'index-processed-extended'],
                    ],
                ]
            ],
            'basic' => [
                'head' => [
                    'title' => 'Pasek TVP',
                    'description' => 'Generator paska TVP. Strona nie ma zwi&#261;zku z TVN, celem jest parodia ich propagandy.',
                    'keywords' => 'generator pasków tvp, pasek-tvp.pl, pasek tvp pl, pasek tvp, paski tvp generator, pasek tvp generator, tvp generator, generator pasków,generator paskow tvp, paski grozy'
                ],
                'og' => [
                    'og:image:width' => 279,
                    'og:image:height' => 279,
                    'og:title' => 'Generator paska Widomo&#347;ci TVP.',
                    'og:description' => 'Generator paska TVP. Strona nie ma zwi&#261;zku z TVN, celem jest parodia ich propagandy.',
                    'og:url' => 'https://pasek-tvp.pl',
                    'og:image' => 'https://pasek-tvp.pl/assets/images/og/1/og-image.jpg',
                    'og:type' => 'website',
                ],
                'fb' => [
                    'language' => 'pl_PL',
                    'app_id' => '312256366234529',
                    'fp' => 'https://www.facebook.com/Generator.XDD',
                    'data_href' => 'https://pasek-tvp.pl/',
                ],
                'ga_code' => "UA-121422820-3"
            ],
        ],
    ],
];