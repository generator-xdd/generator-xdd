<?php


use Phinx\Seed\AbstractSeed;

class SitesSeeder extends AbstractSeed
{
    public function run()
    {
        $data = [
            [
                'uid' => 'site-001',
                'site_id' => 'pasek-tvp',
                'application_uid' => 'app-001',
                'status' => 1,
                'created' => date('Y-m-d H:i:s'),
            ],
        ];

        $table = $this->table('site');
        $table->truncate();
        $table->insert($data)
            ->save();
    }
}
