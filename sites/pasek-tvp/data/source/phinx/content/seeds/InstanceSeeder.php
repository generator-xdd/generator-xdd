<?php


use Phinx\Seed\AbstractSeed;

class InstanceSeeder extends AbstractSeed
{
    public function run()
    {
        $data = [
            [
                'uid'    => 'instance-001',
                'application_uid'    => 'app-001',
                'site_uid'    => 'site-001',
                'client_uid'    => 'ponad-podzialami-client',
                'site_id'    => 'pasek-tvp',
                'status'    => 1,
                'created'    => date('Y-m-d H:i:s'),
            ],
        ];

        $table = $this->table('instances');

        $table->truncate();

        $table->insert($data)
            ->save();
    }
}
