<?php


use Phinx\Seed\AbstractSeed;

class AreaSeeder extends AbstractSeed
{
    public function run()
    {
        $data = [
            [
                'uid' => 'area-001',
                'template_uid' => 'template-001',
                'machine_name' => 'content_main',
                'scope' => 'page',
                'attributes' => '{}',
                'parameters' => '{}',
                'options' => '{}',
                'status' => 1,
                'comm' => 'Homepage',
                'order' => 1,
                'created' => date('Y-m-d H:i:s'),
            ],
            // Processed
            [
                'uid' => 'area-002',
                'template_uid' => 'template-002',
                'machine_name' => 'content_main',
                'scope' => 'page',
                'attributes' => '{}',
                'parameters' => '{}',
                'options' => '{}',
                'status' => 1,
                'order' => 1,
                'comm' => 'Processed Public',
                'created' => date('Y-m-d H:i:s'),
            ],
        ];

        $table = $this->table('area');
        $table->truncate();
        $table->insert($data)
            ->save();
    }
}
