# Vagrant for Generator-xdd Application #

Seems to be broken :/

### environment ###
* bento/ubuntu - Ubuntu 16.04
* Apache2
* PHP 7.2
* xDebug (with configuration)
* Composer
* home directory set to `/var/www/application/`

To access the application open one of the following addresses:
* host IP `192.168.1.84`
* host URL `pasek-tvp.local.vm`
* host port `localhost:8181`

In order to run Vagrant on your development machine you will need to install [Virtualbox](https://www.virtualbox.org/wiki/Downloads) and [vagrant](https://www.vagrantup.com/docs/installation/) with [vagrant-hostpdater](https://github.com/cogitatio/vagrant-hostsupdater) plugin.